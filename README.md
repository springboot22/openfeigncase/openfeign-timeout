**测试代码版本**
```xml
<parent>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-parent</artifactId>
  <version>2.7.9</version>
  <relativePath/> <!-- lookup parent from repository -->
</parent>

<spring-cloud.version>2021.0.6</spring-cloud.version>
```

**设置超时需要特别注意，手动创建的FeignClient 默认有五次重试，所以导致你配置的时间与预期不符，甚至无法判断，到底生效了没。自动装配的配置类里面看到默认时不重试的，所以不是手动创建的就不用管。**

---

<a name="pldMW"></a>
# 一、Request.Options
<a name="Qt5a6"></a>
## 同一个FeignClient下，对不同的API设置不同的超时时间
```java
	@FeignClient(value = "222", url = "http://127.0.0.1:8080")
    public interface OpenClient {

        @RequestLine("GET /api/test/v1")
        String test(URI uri);

        /**
		 * 在调用接口时，手动创建Request.Options 对象，就可以针对单个API设置超时时间
        **/
        @RequestLine("GET /api/test/wait")
        String testW(URI uri,Request.Options options);
    }
```

<a name="VhTwf"></a>
## 手动创建FeignClient 时，将Options参数传进去
```java
	@Data
    @Configuration
    @Import(FeignClientsConfiguration.class)
    public class FeignService {

        private OpenClient openClient;
        
        public Request.Options options(){
            return new Request.Options(1, TimeUnit.SECONDS,1,TimeUnit.SECONDS,true);
        }
        public FeignService(Encoder encoder,Decoder decoder) {
            // 创建Client对象时直接将options参数传进去
            openClient = Feign.builder()
                .options(options())
                // !!!! 注意需要设置重试次数
                .retryer(Retryer.NEVER_RETRY)
                .encoder(encoder).decoder(decoder).target(Target.EmptyTarget.create(OpenClient.class));
        }
    }
```

<a name="qOQOb"></a>
# 通过配置类的方式在手动创建FeignClient时不生效
通过验证发现，只有在FeignClient通过容器创建出来拿来使用时，配置类才生效
```java
	@Configuration
    public class FeignTimeOutConfig {
        @Bean
        public Request.Options options() {
            return new Request.Options(1, TimeUnit.SECONDS, 1, TimeUnit.SECONDS, true);
        }
    }
```
**然后在FeignClient注解上的configuration属性配置上该配置类**<br />**如果这个配置类加上 @Configuration注解的话，就会变成全局配置，可以不在FeignClient上面配置，也可以不加该注解，只在需要的类上配置。**

<a name="PbdS8"></a>
## 通过配置指定的key value方式全局生效
**这种还没进行测试，应该也是无法在手动创建时生效,只能在容器帮我们创建时去自动读取装配**
```yaml
feign:
	client:
		config:
			default: #这里就是指的所有被加载的默认FeignClient实现的服务配置都生效
				connectTimeout: 111000
				readTimeout: 111000
```

<a name="hdjmd"></a>
# 二、RequestInterceptor配置
在apply方法中判断url,然后设置options但是在这个版本中，我无法从参数中获取设置options的方法
