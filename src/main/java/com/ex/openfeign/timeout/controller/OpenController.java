package com.ex.openfeign.timeout.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class OpenController {

    @GetMapping("/test/v1")
    public Object test(){
        return "sucece";
    }

    @GetMapping("/test/wait")
    public Object testWait() throws InterruptedException {
        Thread.sleep(12000);
        return "sucece";
    }
}
