package com.ex.openfeign.timeout.controller;

import com.ex.openfeign.timeout.service.FeignService;
import feign.Request;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/api/test")
@Slf4j
public class TestController {

    @Autowired
    FeignService feignService;

    public Request.Options options() {
        return new Request.Options(1, TimeUnit.SECONDS, 1, TimeUnit.SECONDS, true);
    }

    @GetMapping
    public Object test() {
        return feignService.getOpenClient().testW(URI.create("http://127.0.0.1:8080"));
    }

    @GetMapping("/v2")
    public Object test2() {
        long start = System.currentTimeMillis();
        String test = "";
        try {
           test  = feignService.getOpenClient().testW(URI.create("http://127.0.0.1:8080"));
        }catch (Exception e) {
            log.info("exception:{}",e.getMessage());
            e.printStackTrace();

        }
        long end = System.currentTimeMillis();
        log.info("yong shi:{}",end - start);
        return test;
    }
}
