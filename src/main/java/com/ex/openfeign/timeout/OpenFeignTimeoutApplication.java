package com.ex.openfeign.timeout;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class OpenFeignTimeoutApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenFeignTimeoutApplication.class, args);
    }

}
