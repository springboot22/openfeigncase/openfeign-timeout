package com.ex.openfeign.timeout.service;

import com.ex.openfeign.timeout.feign.OpenClient;
import com.ex.openfeign.timeout.interceptor.FeignTimeoutRequestInterceptor;
import feign.Feign;
import feign.Request;
import feign.Retryer;
import feign.Target;
import feign.codec.Decoder;
import feign.codec.Encoder;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Data
@Configuration
@Import(FeignClientsConfiguration.class)
public class FeignService {

//    @Bean
    public Request.Options options() {
        return new Request.Options(1, TimeUnit.SECONDS,1,TimeUnit.SECONDS,true);
    }

//    @Autowired
    private OpenClient openClient;

    public FeignService(Encoder encoder,Decoder decoder) {
        openClient = Feign.builder()
                .options(options())
                .requestInterceptor(new FeignTimeoutRequestInterceptor())
                .retryer(Retryer.NEVER_RETRY)
                .encoder(encoder).decoder(decoder).target(Target.EmptyTarget.create(OpenClient.class));
    }
}
