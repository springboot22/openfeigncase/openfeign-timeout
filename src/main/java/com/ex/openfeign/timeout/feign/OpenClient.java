package com.ex.openfeign.timeout.feign;

import com.ex.openfeign.timeout.config.FeignTimeOutConfig;
import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.net.URI;

@FeignClient(name = "222", url = "aa",configuration = FeignTimeOutConfig.class)
public interface OpenClient {

    @RequestLine("GET /api/test/v1")
    String test(URI uri);

    @RequestLine("GET /api/test/wait")
    String testW(URI uri);
}
